﻿using GraphBuild;

namespace GraphHasCycles
{
    public class GraphCycles
    {
        static void Main(string[] args)
        {
            Graph graph = new Graph(7);
            
            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 0);
            graph.AddEdge(2, 3);
            graph.AddEdge(3, 3);
            graph.AddEdge(3, 4);
   

            if (graph.HasCycle())
                Console.WriteLine("The graph contains a cycle.");
            else
                Console.WriteLine("The graph has no cycle.");
        }
    }
}