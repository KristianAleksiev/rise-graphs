﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphBuild
{
    public class Graph
    {
        private int sizeOfMatrix;
        private List<int>[] adjecentMatrix;

        public Graph(int size)
        {
            sizeOfMatrix = size;
            adjecentMatrix = new List<int>[sizeOfMatrix];

            for (int index = 0; index < sizeOfMatrix; index++)
            {
                adjecentMatrix[index] = new List<int>();
            }
        }

        public void AddEdge(int startIndex, int endIndex)
        {
            adjecentMatrix[startIndex].Add(endIndex);
            adjecentMatrix[endIndex].Add(startIndex);
        }

        private bool depthFirstSearch(int startNode, bool[] visited, bool[] log)
        {
            visited[startNode] = true;
            log[startNode] = true;

            foreach (int index in adjecentMatrix[startNode])
            {
                if (!visited[index])
                {
                    if (depthFirstSearch(index, visited, log))
                        return true;
                }
                else if (log[index])
                    return true;
            }

            log[startNode] = false;
            return false;
        }


        public bool HasCycle()
        {
            bool[] visitedArr = new bool[sizeOfMatrix];
            bool[] logStack = new bool[sizeOfMatrix];

            for (int index = 0; index < sizeOfMatrix; index++)
            {
                if (!visitedArr[index])
                {
                    if (depthFirstSearch(index, visitedArr, logStack))
                        return true;
                }
            }
            return false;
        }
    }
}
